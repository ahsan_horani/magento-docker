#Remove all containers
docker rm -f "$(docker ps -a -q)"

#Cleanup networks
docker network prune -f

#Cleanup volumes
docker network volume -f