#!/bin/bash

#Stop all
docker stop $(docker ps -a -q)

#Remove all containers
docker rm -f "$(docker ps -a -q)"